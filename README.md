Description
===========
This proxy can be used to configure firewall rules based on dns traffic.

## Command line flags
    --c string   Path to configuration file (default "$HOME/.firewallProxy/config.json")
    --d          Print all log messages to stdout instead of using a logfile
    --f string   Logfile path (default "/var/log/firewallProxy/{timestamp}.log")
    --l int      Log level (0 - CRITICAL .. 5 - DEBUG (default 4)

## Example configuration file
```
{
    "nameserver": ["8.8.8.8:53"],                       // primary name server(s) to use (they should be specified by IP and port and not by domain to ensure all dns requests can be resolved)
    "rules": [
        {
            "domains": [                                // list of domains for which this rule should be applied
                "facebook.com",                         // concrete domain name
                "*.youtbe.com",                         // wildcard for one level (matches e.g. www.youtube.com, mobile.youtube.com,...)
                "**.google.com"                         // wildcard for all levels (matches e.g. www.google.com, youtube-ui.l.google.com,...)
            ],
            "reject": false,                            // forward requests to upstream DNS server
            "source_nets": [                            // list of source IPs / networks this rule should be restricted to
                "127.0.0.1",
                "192.168.35.0/24"
            ],
            "on_resolve": {
                "exec": "sudo ufw deny out to {ip}",    // Excute this command after the upstream DNS server has resolved the domain name ({ip} is the IP of the destination host)
                "ip_delimiter": ""                      // In case there exist multiple IPs for a specific domain a delimiter can be specified. {ip} is then a list of IPs splitted by the delimiter.
            },                                          // If no delimiter is specified and there are multiple IPs for the matched domain, then the specified command will be executed multiple times
            "nameserver":"doh:dns.google"               // Optional nameserver that should be used for this rule. Can be specified by IP or by domain. In this case dhcp over https (doh) is used.
        },
        {
            "domains": [
                "**.example.com"
            ],
            "reject": true,                             // reject all requests from localhost to all subdomains of example.com
            "source_nets": [
                "127.0.0.1/255.255.255.255"
            ],
            "nameserver": "udp:9.9.9.9"                 // Nameservers can be prefixed with "udp:" and without port. In this case the standard port 53 is used.
        }
    ]
} 
```

## Slots to be used as command line arguments ("exec")

- {ip}                      ... IP address returned by the DNS Server
- {host}                    ... fully qualified host name returned by the DNS Server (e.g. www.google.com)
- {source_net_ip}           ... IP address of the client that made the DNS request
- {source_net}              ... network part of IP address of the requesting client
- {source_net_mask}         ... network mask (if specified in "source_net")
- {source_net_mask_length}  ... number of bits that mask the network within the IP address