package main

import (
	"crypto/md5"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/json"
	"github.com/knadh/koanf/providers/file"
	"github.com/knadh/koanf/providers/posflag"
	"github.com/miekg/dns"
	"github.com/op/go-logging"
	"github.com/spf13/pflag"
)

const (
	LOGLEVEL = 4
)

var (
	k          *koanf.Koanf
	configFile *file.File
	config     Config
	debugMode  bool
	log        = logging.MustGetLogger("firewallProxy")
	logFormat  = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} %{level:.4s} ▶ %{color:reset} %{message}`,
	)
)

type Rule struct {
	Domains   []string `koanf:"domains"`
	Reject    bool     `koanf:"reject"`
	OnResolve struct {
		Exec        string `koanf:"exec"`
		IpDelimiter string `koanf:"ip_delimiter"`
	} `koanf:"on_resolve"`
	SourceNets []string `koanf:"source_nets"`
	Nameserver []string `koanf:"nameserver"`
}

type Config struct {
	Nameserver []string `koanf:"nameserver"`
	Rules      []Rule   `koanf:"rules"`
}

/******************************************
* UTILITY FUNCTIONS
*******************************************/

func createDirectory(path string) error {

	if _, err := os.Stat(path); err != nil {

		if os.IsNotExist(err) {

			err = os.MkdirAll(path, 0755)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}
	return nil
}

func initLogger(filePath string, logLevel int) error {

	var logBackend logging.Backend
	if debugMode {

		fmt.Printf("Logfile: stdout\n")
		logBackend = logging.NewLogBackend(os.Stdout, "", 0)
	} else {

		var dirPath = filePath[:strings.LastIndex(filePath, "/")]
		if err := createDirectory(dirPath); err != nil {
			return err
		}

		logFile, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			return err
		}

		fmt.Printf("Logfile: %v\n", filePath)
		logBackend = logging.NewLogBackend(logFile, "", 0)
	}

	logBackendFormatter := logging.NewBackendFormatter(logBackend, logFormat)
	logBackendLeveled := logging.AddModuleLevel(logBackendFormatter)
	switch logLevel {
	case 0:
		logBackendLeveled.SetLevel(logging.CRITICAL, "")
	case 1:
		logBackendLeveled.SetLevel(logging.ERROR, "")
	case 2:
		logBackendLeveled.SetLevel(logging.WARNING, "")
	case 3:
		logBackendLeveled.SetLevel(logging.NOTICE, "")
	case 4:
		logBackendLeveled.SetLevel(logging.INFO, "")
	case 5:
		logBackendLeveled.SetLevel(logging.DEBUG, "")
	}
	logging.SetBackend(logBackendLeveled)

	return nil
}

func resolveRules() {

	for _, rule := range config.Rules {

		for i, d := range rule.Domains {
			d = strings.ReplaceAll(d, "-", "\\-")
			d = strings.ReplaceAll(d, ".", "\\.")
			d = strings.ReplaceAll(d, "**", "[a-zA-Z0-9-.]§")
			d = strings.ReplaceAll(d, "*", "[a-zA-Z0-9-]§")
			d = strings.ReplaceAll(d, "§", "*")
			d = "^" + d + "\\.$"
			log.Debugf("Create regexp for domain: %v", d)
			rule.Domains[i] = d
		}
	}
}

func loadConfig(event interface{}, err error) {

	if err != nil {
		log.Error(err)
		return
	}

	//log.Debugf("configFile: %v", configFile)

	if err = k.Load(configFile, json.Parser()); err != nil {
		log.Fatal(err)
		return
	}

	log.Infof("... update configuration ...")
	config = Config{}
	k.Unmarshal("", &config)
	log.Debugf("%v", config)
	resolveRules()
}

func main() {

	flag := pflag.NewFlagSet("", pflag.ContinueOnError)
	flag.Usage = func() {
		var description = `'This proxy can be used to configure firewall rules based on dns traffic.'`

		fmt.Printf("\n%v\n\n", description)
		fmt.Printf("Flags:\n")
		flag.PrintDefaults()
		os.Exit(0)
	}

	var homePath = os.Getenv("HOME")
	if runtime.GOOS == "windows" {
		homePath = os.Getenv("HOMEPATH")
	}

	var configFilePath = homePath + "/.firewallProxy/config.json"

	var logfilePath = "/var/log/firewallProxy/" + time.Now().Format("20060102150405") + ".log"
	if runtime.GOOS == "windows" {
		logfilePath = os.Getenv("LOCALAPPDATA") + "/firewallProxy/" + time.Now().Format("20060102150405") + ".log"
	}

	// parse cli arguments
	flag.String("c", configFilePath, "Path to config file.")
	flag.Int("l", LOGLEVEL, "Log level (0 - CRITICAL .. 5 - DEBUG")
	flag.String("f", logfilePath, "Logfile path")
	flag.Bool("d", false, "Print all log messages to stdout instead of using a logfile")

	flag.Parse(os.Args)

	k = koanf.New(".")
	err := k.Load(posflag.Provider(flag, "_", k), nil)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error loading config from flags: %s", err)
		os.Exit(1)
	}

	configPath := k.String("c")
	logLevel := k.Int("l")
	logPath := k.String("f")
	debugMode = k.Bool("d")

	// set log level to debug, if debugmode is enabled
	if debugMode {
		logLevel = 5
	}

	// create logger
	err = initLogger(logPath, logLevel)
	if err != nil {
		err = initLogger(homePath+"/.firewallProxy/log/"+time.Now().Format("20060102150405")+".log", logLevel)
		if err != nil {
			log.Fatal(err.Error())
		}
	}

	// load config and register watcher
	configFile = file.Provider(configPath)
	configFile.Watch(loadConfig)
	loadConfig(nil, nil)

	// attach request handler func
	dns.HandleFunc(".", handleDNSRequest)

	// start server
	server := &dns.Server{
		Addr: ":53",
		Net:  "udp",
	}

	log.Infof("Started dns server on port 53\n")
	err = server.ListenAndServe()
	if err != nil {
		log.Fatalf("Failed to start dns server: %v\n ", err.Error())
	}

	defer server.Shutdown()
}

func fowardNoIntercept(w dns.ResponseWriter, r *dns.Msg) {

	var dnsResponse = forwardQuery(r, []string{})
	if dnsResponse == nil {
		log.Infof("[%v] Upstream dns query failed\n", r.Id)
		var msg = &dns.Msg{}
		msg.SetReply(r)
		msg.SetRcode(r, dns.RcodeServerFailure)
		w.WriteMsg(msg)
		return
	}

	// respond to query
	dnsResponse.SetReply(r)
	w.WriteMsg(dnsResponse)
}

func handleDNSRequest(w dns.ResponseWriter, r *dns.Msg) {

	var (
		err         error
		sourceIP    net.IP
		sourceNet   *net.IPNet
		dnsResponse *dns.Msg
		applyRule   bool
	)

	if r.Opcode != dns.OpcodeQuery {
		log.Infof("[%v] Request not of type query: %v\n", r.Id, r.Opcode)
		fowardNoIntercept(w, r)
		return
	}

	// Practically only first question is answered
	// see: https://tools.ietf.org/html/draft-bellis-dnsext-multi-qtypes-03
	var q = r.Question[0]
	if q.Qtype != dns.TypeA {
		log.Debugf("[%v] Question not of type A: %v\n", r.Id, q.Qtype)
		fowardNoIntercept(w, r)
		return
	}

	var domain = strings.ToLower(q.Name)

	log.Infof("[%v] Lookup: %v\n", r.Id, domain)

	var rule = matchRule(domain)

	// rule found?
	if rule != nil {

		// check IP
		applyRule = len(rule.SourceNets) == 0
		if len(rule.SourceNets) > 0 {
			sourceIP = net.ParseIP(strings.Split(w.RemoteAddr().String(), ":")[0])
			for _, n := range rule.SourceNets {
				// add netmask if ip
				if !strings.Contains(n, "/") {
					n += "/32"
				}
				_, sourceNet, err = net.ParseCIDR(n)
				if err != nil {
					var splits = strings.Split(n, "/")
					var ip [4]byte
					for idx, s := range strings.Split(splits[0], ".") {
						var i int64
						if i, err = strconv.ParseInt(s, 10, 64); err != nil {
							log.Errorf("e1: %v", err)
						}
						ip[idx] = byte(i)
					}

					var mask [4]byte
					for idx, s := range strings.Split(splits[1], ".") {
						var i int64
						if i, err = strconv.ParseInt(s, 10, 64); err != nil {
							log.Errorf("e2: %v", err)
						}
						mask[idx] = byte(i)
					}

					sourceNet = &net.IPNet{IP: net.IPv4(ip[0], ip[1], ip[2], ip[3]), Mask: net.IPv4Mask(mask[0], mask[1], mask[2], mask[3])}
				}
				if sourceNet.Contains(sourceIP) {
					log.Debugf("[%v] %v in network %v\n", r.Id, sourceIP, sourceNet)
					applyRule = true
					break
				}
			}
		}
	}

	// apply rule & reject ?
	if applyRule && rule.Reject {
		log.Infof("[%v] Reject query\n", r.Id)
		var msg = &dns.Msg{}
		msg.SetReply(r)
		msg.SetRcode(r, dns.RcodeRefused)
		w.WriteMsg(msg)
		return
	}

	// forward to upstream dns server
	if applyRule && len(rule.Nameserver) > 0 {
		dnsResponse = forwardQuery(r, rule.Nameserver) // use custom nameserver for query
	} else {
		dnsResponse = forwardQuery(r, []string{})
	}
	if dnsResponse == nil || dnsResponse.Rcode != dns.RcodeSuccess {
		log.Infof("[%v] Upstream dns query failed\n", r.Id)
		var msg = &dns.Msg{}
		msg.SetReply(r)
		msg.SetRcode(r, dns.RcodeServerFailure)
		w.WriteMsg(msg)
		return
	}

	//log.Debugf("[%d] on_resolve: %v", r.Id, rule)

	// apply rule & execute command ?
	if applyRule && len(rule.OnResolve.Exec) > 0 {
		var host string
		var ips []string
		for _, rr := range dnsResponse.Answer {
			if rr.Header().Rrtype == dns.TypeA {
				var splits = strings.Split(rr.String(), "\t")
				host = splits[0][:len(splits[0])-1]
				ips = append(ips, splits[4])
			}
		}

		if len(ips) > 0 {

			if len(rule.OnResolve.IpDelimiter) > 0 {
				var ipToken = strings.Join(ips, rule.OnResolve.IpDelimiter)
				var command = strings.ReplaceAll(rule.OnResolve.Exec, "{ip}", ipToken)
				command = strings.ReplaceAll(command, "{host}", host)
				if sourceIP != nil {
					command = strings.ReplaceAll(command, "{source_net_ip}", sourceIP.String())
				}
				if sourceNet != nil {
					command = strings.ReplaceAll(command, "{source_net}", sourceNet.String())
					command = strings.ReplaceAll(command, "{source_net_mask}", ipv4MaskString(sourceNet.Mask))
					command = strings.ReplaceAll(command, "{source_net_mask_length}", strings.Split(sourceNet.String(), "/")[1])
				}
				execute(command, r.Id)
			} else {
				// execute command for every ip
				for _, ip := range ips {
					var command = strings.ReplaceAll(rule.OnResolve.Exec, "{ip}", ip)
					command = strings.ReplaceAll(command, "{host}", host)
					if sourceIP != nil {
						command = strings.ReplaceAll(command, "{source_net_ip}", sourceIP.String())
					}
					if sourceNet != nil {
						command = strings.ReplaceAll(command, "{source_net}", sourceNet.String())
						command = strings.ReplaceAll(command, "{source_net_mask}", ipv4MaskString(sourceNet.Mask))
						command = strings.ReplaceAll(command, "{source_net_mask_length}", strings.Split(sourceNet.String(), "/")[1])
					}
					execute(command, r.Id)
				}
			}
		}
	}

	// respond to query
	dnsResponse.SetReply(r)
	w.WriteMsg(dnsResponse)
}

var dnsClient = new(dns.Client)

func forwardQuery(req *dns.Msg, nameserver []string) *dns.Msg {

	var nsIdx int
	var r *dns.Msg
	//var d time.Duration
	var err error

	// nameservers of rule before global nameservers
	var nservers = append(nameserver, config.Nameserver...)

	if len(nservers) == 0 {
		log.Fatalf("please specify at least one nameserver in config.json")
	}

	for try := 1; try <= 3; try++ {
		log.Debugf("[%d] Send to upstream server %s | type %v",
			req.Id, nservers[nsIdx], req.Opcode)

		var nameserver = nservers[nsIdx]
		var doDOHRequest = false

		if strings.HasPrefix(nameserver, "doh:") {
			nameserver = nameserver[4:]
			doDOHRequest = true
		} else if strings.HasPrefix(nameserver, "udp:") {
			nameserver = nameserver[4:]
		}

		//log.Debugf("DNS Query: %v", req)

		if doDOHRequest {
			r, err = dohRequest(req, nameserver)
		} else {
			if !strings.HasSuffix(nameserver, "53") {
				nameserver += ":53"
			}
			r, _, err = dnsClient.Exchange(req, nameserver)
		}

		//log.Debugf("[%d] Response duration: %s", req.Id, d)

		if err != nil {
			log.Errorf("[%d] Failed to query upstream server '%s': %v",
				req.Id, nameserver, err)

			// Continue with next available server
			if len(nservers)-1 > nsIdx {
				nsIdx++
			} else {
				nsIdx = 0
			}
			continue
		}

		log.Debugf("[%d] Response code: %s", req.Id, dns.RcodeToString[r.Rcode])
		break
	}

	return r
}

var httpClient = &http.Client{
	Timeout: time.Second,
}

func dohRequest(req *dns.Msg, nameserver string) (*dns.Msg, error) {

	var r *dns.Msg
	var reqData, respData []byte
	var httpReq *http.Request
	var httpResp *http.Response
	var err error

	reqData, err = req.Pack()
	if err != nil {
		return nil, err
	}

	var url = nameserver
	if !strings.HasPrefix(url, "http") {
		url = "https://" + url
	}
	if !strings.HasSuffix(url, "/dns-query") {
		url += "/dns-query"
	}

	//log.Debugf("DOH request to %v", url)

	// GET REQUEST (better cacheable and therefor lower latency)
	var dq = base64.RawURLEncoding.EncodeToString(reqData)
	url = fmt.Sprintf("%s?dns=%s", url, dq)
	//log.Debugf("Query: %v", url)
	httpReq, err = http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// POST REQUEST
	/*
		httpReq, err = http.NewRequest("POST", url, bytes.NewReader(reqData))
		if err != nil {
			return nil, err
		}
		httpReq.Header.Add("Content-Type", "application/dns-message")
	*/

	httpResp, err = httpClient.Do(httpReq)
	if err != nil {
		return nil, err
	}

	if httpResp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("DOH request to %v failed with status %v - %v", nameserver, httpResp.StatusCode, httpResp.Status))
	}

	defer httpResp.Body.Close()

	respData, err = ioutil.ReadAll(httpResp.Body)
	if err != nil {
		return nil, err
	}

	//log.Debugf("doh server response: %s", respData)

	r = new(dns.Msg)
	err = r.Unpack(respData)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func matchRule(domain string) *Rule {

	for ridx, rule := range config.Rules {
		for _, d := range rule.Domains {

			//log.Debugf("%v match rule: %v\n", domain, d)

			matched, err := regexp.MatchString(d, domain)
			if err != nil {
				log.Error(err)
				continue
			}
			if matched {
				log.Debugf("%v matched rule %v: %v\n", domain, ridx, d)
				return &rule
			}
		}
	}
	return nil
}

func isIPInNetwork(networks []string, _ip string) bool {

	var ip = net.ParseIP(_ip)

	for _, n := range networks {
		_, ipnet, err := net.ParseCIDR(n)
		if err != nil {
			log.Error(err)
			continue
		}

		if ipnet.Contains(ip) {
			return true
		}
	}

	return false
}

func ipv4MaskString(m []byte) string {
	if len(m) != 4 {
		panic("ipv4Mask: len must be 4 bytes")
	}
	return fmt.Sprintf("%d.%d.%d.%d", m[0], m[1], m[2], m[3])
}

// Hash of executed commands
var cmdHistory = map[string]bool{}

func execute(command string, dnsRequestId uint16) {
	if cmdHistory[hash(command)] {
		log.Debugf("Skip command '%v'", command)
		return
	}
	cmdHistory[hash(command)] = true

	log.Infof("[%v] Execute: %v", dnsRequestId, command)

	var cmd *exec.Cmd
	switch runtime.GOOS {
	case "windows":
		cmd = exec.Command("cmd", "/C", command)
	default:
		cmd = exec.Command("sh", "-c", command)

	}
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		log.Errorf("[%v] %v", dnsRequestId, err)
	}
	log.Infof("[%v] Result: %s", dnsRequestId, stdoutStderr)

	// log command result
	/*
		go func() {
			if err := cmd.Wait(); err != nil {
				log.Errorf("%v: %s", err, stderr.String())
			}
		}()
	*/
}

func hash(text string) string {
	h := md5.New()
	io.WriteString(h, text)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func writeMsg(w dns.ResponseWriter, m *dns.Msg) {
	if err := w.WriteMsg(m); err != nil {
		log.Errorf("[%d] Failed to return reply: %v", m.Id, err)
	}
}
